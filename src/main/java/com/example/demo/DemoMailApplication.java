package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

@EnableScheduling
@SpringBootApplication
public class DemoMailApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMailApplication.class, args);
        System.out.println("Hello");
        sendMail();
    }

    @Scheduled(cron = "15 * * ? * *")
    public void run() {
        System.out.println("Current time is :: " + Calendar.getInstance().getTime());
    }

    public static void sendMail() {
        //Setting up configurations for the email connection to the Google SMTP server using TLS
        System.out.println("hello2");
        Properties props = new Properties();
        props.put("mail.smtp.host", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        //Establishing a session with required user details
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("nadimul@divine-it.net", "zkqpwhpsjgbnyjzs");
            }
        });
        try {

            //Storing the comma seperated values to email addresses
            String to = "nadim.hq321@gmail.com";

            //Creating a Message object to set the email content
            MimeMessage msg = new MimeMessage(session);


            // creates message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent("<span class=\"gmail-com\" style=\"box-sizing: inherit; color: rgb(216, 27, 96);\">/*** Send an email from the user's mailbox to its recipient.<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;*<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;* @param service Authorized Gmail API instance.<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;* @param userId User's email address. The special value \"me\"<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;* can be used to indicate the authenticated user.<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;* @param emailContent Email to be sent.<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;* @return The sent message<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;* @throws MessagingException<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;* @throws IOException<br style=\"box-sizing: inherit;\">&nbsp; &nbsp; &nbsp;*/</span>", "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);


            InternetAddress[] address = InternetAddress.parse(to, true);
            //Setting the recepients from the address variable
            msg.setRecipients(Message.RecipientType.TO, address);
            String timeStamp = new SimpleDateFormat("yyyymmdd_hh-mm-ss").format(new Date());
            msg.setSubject("Sample Mail : " + timeStamp);
            msg.setSentDate(new Date());
//            msg.setText("Sampel System Generated mail");
            msg.setHeader("XPriority", "1");
            msg.setContent(multipart);
            Transport.send(msg);
            System.out.println("Mail has been sent successfully");
        } catch (MessagingException mex) {
            System.out.println("Unable to send an email" + mex);
        }
    }

}